<?php 
require_once "./php-crm-sdk/AlexaSDK_Abstract.php";
require_once "./php-crm-sdk/config.php";

if (isset($_POST["username"]) && isset($_POST["password"])){
	
		$client = new AlexaSDK($settings);
	
		extract($_POST);
		/* Construct parameters for solution's alex_ValidateLogin custom action */
		$parameters = array(
			array('key' => 'Login',
				  'value' => $username,
				  'type' => 'string'
				),
			array('key' => 'Password',
				  'value' => $password,
				  'type' => 'string'
				)
		);
		
		/* Execute custom action */
		$executeActionResponse = $client->executeAction('alex_ValidateLogin', $parameters);
		
		if ($executeActionResponse->IsValid == "true"){
				setcookie(  "login_session", 
							json_encode((array)$executeActionResponse), 
							(time() + 2 * 86400));
		}else{
			$error = true;
		}
	
}

$session = (isset($_COOKIE["login_session"])) ? json_decode($_COOKIE["login_session"]) : null;
?>

<?php include("header.php"); ?>

<!-- Page Title -->
		<div class="section section-breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Login</h1>
					</div>
				</div>
			</div>
		</div>
        <div class="section">
	    	<div class="container">
				<div class="row">
					<div class="col-sm-5">
						
						<?php if (!$session) : ?>
						
							<div class="basic-login">
								<form role="form" role="form" method="post">
									<?php if ($error) : ?>
										<div id="login-message" class="alert alert-danger" role="alert">Incorrect login or password</div>
									<?php endif; ?>
									<div class="form-group">
										<label for="login-username"><i class="icon-user"></i> <b>Username or Email</b></label>
										<input class="form-control" name="username" id="login-username" type="text" placeholder="">
									</div>
									<div class="form-group">
										<label for="login-password"><i class="icon-lock"></i> <b>Password</b></label>
										<input class="form-control" name="password" id="login-password" type="password" placeholder="">
									</div>
									<div class="form-group">
										<label class="checkbox">
											<input type="checkbox"> Remember me
										</label>
										<a href="page-password-reset.html" class="forgot-password">Forgot password?</a>
										<button type="submit" class="btn pull-right">Login</button>
										<div class="clearfix"></div>
									</div>
								</form>
							</div>
						<?php else : ?>
						
							<p>Logged in</p>
						
						<?php endif; ?>
						
					</div>
				</div>
			</div>
		</div>

<?php include("footer.php"); ?>